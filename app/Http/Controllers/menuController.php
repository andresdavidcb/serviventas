<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\People;

class menuController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function people(){
        return view('people.index');
    }
    // public function sql(){
    //     $model=People::all();
    //     return $model;
    // }
}
