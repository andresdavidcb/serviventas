<?php
use App\Models\People;
use Faker\Generator as Faker;

$factory->define(People::class, function (Faker $faker) {
    return [
        'name'=>$faker->name,
        'lastname'=>$faker->lastname,
        'address'=>$faker->address,
    ];
});
