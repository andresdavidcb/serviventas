<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePropertiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('properties', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('address');
            $table->bigInteger('owner');
            $table->decimal('lot_area',12,2);
            $table->decimal('built_area',12,2);
            $table->smallInteger('rooms');
            //$table->smallInteger('bathrooms');
            $table->smallInteger('internal_bathrooms');
            $table->smallInteger('social_bathrooms');
            $table->boolean('work_area');
            $table->smallInteger('dinning_rooms');
            $table->boolean('parking_area');
            $table->boolean('garage');
            $table->longText('description');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('properties');
    }
}
