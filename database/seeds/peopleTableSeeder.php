<?php
use App\Models\People;
use Illuminate\Database\Seeder;

class peopleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $people=factory(People::class,20)->create();
    }
}
